package test;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
 
public class New extends JFrame implements ActionListener {
 
    JButton ajouter = new JButton("Ajouter");
    JButton supprimer = new JButton("Supprimer");
    ArrayList<JButton> listBoutons = new ArrayList<JButton>();
    JPanel panelCentre = new JPanel();
 
    public New() {
        setSize(400,400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panelNord = new JPanel();
        ajouter.addActionListener(this);
        supprimer.addActionListener(this);
        panelNord.add(ajouter);
        panelNord.add(supprimer);
        add(panelNord,BorderLayout.NORTH);
        add(panelCentre,BorderLayout.CENTER);
        setVisible(true);
    }
 
    public static void main(String[] args) {
        New tb = new New();
    }
 
    public void actionPerformed (ActionEvent e) {
        if (e.getSource().equals(ajouter)) {
            listBoutons.add(new JButton("bouton" + (listBoutons.size()+1)));
            panelCentre.add(listBoutons.get(listBoutons.size()-1));
            panelCentre.revalidate();
        }
        else {
            if (listBoutons.size()>0) {   // s'il y a des bouton on supprime
                panelCentre.remove(listBoutons.get(listBoutons.size()-1));
                listBoutons.remove(listBoutons.size()-1);
                panelCentre.repaint();
            }
        }
    }
}
