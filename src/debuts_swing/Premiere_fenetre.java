package debuts_swing;

import javax.swing.*;
import java.awt.FlowLayout;
import java.awt.Event.*;


public class Premiere_fenetre {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		JFrame fen = new JFrame();
		 //Définit sa taille : 300 pixels de large et 150 pixels de haut
		fen.setSize(300, 150);
		//Définit un titre pour notre fenêtre
		fen.setTitle("Ma première fenetre");
		//La rend visible 
		fen.setVisible(true);
		//Positionner au centre
		fen.setLocationRelativeTo(null);

		DeuseFenetre fen2 = new DeuseFenetre();
		fen2.setVisible(true);
	}
}