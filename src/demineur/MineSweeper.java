package demineur;

//Programme permettant de jouer au demineur
/**
 * @author	Nicolas Duwig
 * Programme permettant de jouer au demineur
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.GridLayout; 
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import java.awt.FlowLayout;



public class MineSweeper implements ActionListener, MouseListener {
	
	int rows =10;
	int cols =10;
	int numMines =10;
	int nbGrille=rows*cols;
	boolean gagne = false;
    boolean perdu = false;
    boolean[] plateauMines = new boolean [rows*cols];
    int[] plateauNums = new int [rows*cols];
    JButton[] tableauBoutons = new JButton[rows*cols];
    GridLayout grille = new GridLayout(rows, cols);

//    JMenuItem difficulty = new JMenuItem("options");
    
    
    
    //main dans lequel on appel la methode MineSweeper pour lancer le démineur
    public static void main(String[] args) {
		// TODO Auto-generated method stub
		new MineSweeper();
		
		}
	
		
	
	//Methode dans laquelle on construit l'affichage du démineur
	public MineSweeper() {

		
		
		//On commence par créer notre fenêtre
		JFrame fen = new JFrame();
//		fen.setLayout(grille);
		fen.setTitle("Demineur");
		fen.setSize(500, 500);
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fen.setLocationRelativeTo(null);
		fen.setResizable(false);
	
		
		
		//On crée un panel qui contiendra le plateau/la grille de jeux
		JPanel panel = new JPanel();
		panel.setLayout(grille);
		fen.setContentPane(panel);
		
		
		//On crée la barre de menu de notre jeux
		JMenuBar myMenuBar=new JMenuBar();
		myMenuBar.setSize(30,500);
		myMenuBar.setBackground(Color.LIGHT_GRAY); 
		//On le fixe dans notre fenetre
		fen.setJMenuBar(myMenuBar);
		
		
		//On crée le menu "Options"
		JMenu menuOptions = new JMenu("Options");
		//puis on le met dans notre barre de menu
		myMenuBar.add(menuOptions);
		
		
		
		//On crée le sous menu "Nouvelle partie"
		JMenuItem menuNouveau = new JMenuItem("Nouvelle partie");
		//On le met dans notre menu "Options"
		menuOptions.add(menuNouveau);
		//On implémente ce qui doit être fait après un click souris
		menuNouveau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new MineSweeper();//Crée une nouvelle fentre ; à changer
				
			}});
		JMenuItem menuDifficulte = new JMenuItem("Difficulte");
		menuOptions.add(menuDifficulte);
		
		
		
		JMenuItem menuQuitter = new JMenuItem("Quitter"); 
		menuOptions.add(menuQuitter);
		menuQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fen.dispose();
			}});
		
		
		
		
		
		
		//On crée nos boutons, on les met dans un tableau de boutons et on les fixent à notre panel
		for ( int i=0; i<nbGrille; i++) {
			tableauBoutons[i]= new JButton();
			panel.add(tableauBoutons[i]);
			//ligne pour que les boutons captent le fait qu'on leur click dessus
			tableauBoutons[i].addActionListener(this);
//			tableauBoutons[i].addMouseListener(this);    //servira quand on voudra distinguer click gauche ou droit 
		}
		
		
		//On rend visible notre fenetre
		fen.setVisible(true);
		
		
		
		//On donne des coordonnées x et y à nos mines de manière aléatoire  
		for (int i=0; i<numMines; i++) {
			int coordXMine = (int) Math.floor(Math.random() * rows);
	        int coordYMine = (int) Math.floor(Math.random() * cols);
	        //On rentre dans un tableau de booléen, ayant pour taille celle de la grille, la postition relative des mines
	        plateauMines[(rows*coordYMine)+coordXMine]=true;
		}

		
		//On rentre ici dans un tableau les numéros situés autour des mines 
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                int place = (rows * y) + x;
                if (plateauMines[place]) {
                	plateauNums[place] = 0;
                    continue;
                }
                int tampon = 0;
                boolean limiteGauche = (x - 1) >= 0; 
                boolean limiteDroite = (x + 1) < rows;
                boolean limiteHaute = (y - 1) >= 0;
                boolean limiteBasse = (y + 1) < cols;
                int gauche = (rows * (y)) + (x - 1);
                int droite = (rows * (y)) + (x + 1);
                int haut = (rows * (y - 1)) + (x);
                int hautGauche = (rows * (y - 1)) + (x - 1);
                int hautDroite = (rows * (y - 1)) + (x + 1);
                int bas = (rows * (y + 1)) + (x);
                int basGauche = (rows * (y + 1)) + (x - 1);
                int basDroite = (rows * (y + 1)) + (x + 1);
                if (limiteHaute) {
                    if (plateauMines[haut]) {
                    	tampon++;
                    }
                    if (limiteGauche) {
                        if (plateauMines[hautGauche]) {
                        	tampon++;
                        }
                    }
                    if (limiteDroite) {
                        if (plateauMines[hautDroite]) {
                        	tampon++;
                        }
                    }
                }
                if (limiteBasse) {
                    if (plateauMines[bas]) {
                    	tampon++;
                    }
                    if (limiteGauche) {
                        if (plateauMines[basGauche]) {
                        	tampon++;
                        }
                    }
                    if (limiteDroite) {
                        if (plateauMines[basDroite]) {
                        	tampon++;
                        }
                    }
                }
                if (limiteGauche) {
                    if (plateauMines[gauche]) {
                    	tampon++;
                    }
                }
                if (limiteDroite) {
                    if (plateauMines[droite]) {
                    	tampon++;
                    }
                }
                plateauNums[place] = tampon;
            }
        }
    }
		
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		//code selon les événements
		
		
		//On récupère l'action du click souris sur un bouton
		for (int coordXBouton = 0; coordXBouton < rows; coordXBouton++) {
            for (int coordYBouton = 0; coordYBouton < cols; coordYBouton++) {
                if (e.getSource() == tableauBoutons[(rows * coordYBouton) + coordXBouton]) {
                	//Si le bouton a été clické, on le désactive
                	tableauBoutons[(rows * coordYBouton) + coordXBouton].setEnabled(false);         
                }
            }}
		
		
	}
	
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		
		
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
		
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
		
	}

}
