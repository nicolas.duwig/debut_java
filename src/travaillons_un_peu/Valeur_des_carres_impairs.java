package travaillons_un_peu;

import java.util.Scanner;


//programme affichant un triangle isocèle formé d'étoiles

public class Valeur_des_carres_impairs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int n, impaire=1;
		
		
		//On commence par demander le nombre de valeurs souhaitées
		Scanner clavier = new Scanner(System.in);
		System.out.print("combien de valeurs : ");
		n = clavier.nextInt();
		
		//déclaration du tableau
		int [] tab = new int [n];
		
		for (int i=0; i<n; i++) {
			//on met les valeurs des carrés dans le tableau
			tab[i]=impaire*impaire;
			//on affiche la valeur du nombre correspondant
			System.out.println(impaire+" a pour carre "+tab[i]);
			//on passe au nombre impaire suivant
			impaire+=2;
		
		}
	}
}
