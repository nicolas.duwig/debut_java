package travaillons_un_peu;

import java.util.Scanner;

//programme qui indique combien de fois les voyelles sont présentent dans un mot

public class Combien_de_fois {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        int nbra=0, nbre=0, nbry=0, nbru=0, nbri=0 ,nbro=0;
        
        Scanner clavier = new Scanner(System.in);
		System.out.print("donnez un mot : ");
		String mot = clavier.nextLine();
        
   
        //On converti tous les caractères en minuscules
        mot = mot.toLowerCase();
        
        for(int i = 0; i < mot.length(); i++) {
        	//on créer une variable comparant qui servira à comparer les lettre
        	//mot.charAR(i) sert à retourner la position de la lettre
           char comparant = mot.charAt(i); 
           if(comparant == 'a') { 
        	   nbra++;
           }
        	  else if (comparant == 'e') {
        		   nbre++;
        	  }
        	  else if (comparant == 'y') {
        		  nbry++;
        	  }
        	  else if (comparant == 'u') {
        		  nbru++;
        	  }
        	  else if (comparant == 'i') {
        		  nbri++;
        	  }
        	  else if (comparant == 'o') {
        		  nbro++;
        	  }
        	     
           } 
        System.out.println("il comporte");
        System.out.println(nbra+" fois la lettre a");
        System.out.println(nbre+" fois la lettre e");
        System.out.println(nbri+" fois la lettre i");
        System.out.println(nbro+" fois la lettre o");
        System.out.println(nbru+" fois la lettre u");
        System.out.println(nbry+" fois la lettre y");

        }
    }
