package travaillons_un_peu;

import java.util.Scanner;

//programme affichant un triangle isocèle formé d'étoiles
public class Sapin_de_Noel {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int hauteur, etoiles=1;
		
		//On commence par demander le nombre de lignes souhaité en saisie clavier
		Scanner clavier = new Scanner(System.in);
		System.out.print("combien de lignes ? ");
		hauteur = clavier.nextInt();
		
		//boucle pour le nombre de lignes
		for (int i=0; i<hauteur; i++) {
			//boucle pour les espaces avant les étoiles
			//l'étoile se trouvant au millieu sera toujours à la place ayant pour
			//valeur le nombre de ligne ; par exemple sion a 6 lignes, l'étoile 
			//sera à la postion 6 par rapport au bord i.e pour la première il y 
			//aura 5 espaces avant
			for (int j=0; j<(hauteur-(i+1)); j++) {  
				System.out.print(" ");
			}
			//boucle pour les étoiles
			for(int j=0; j<etoiles; j++) {
				System.out.print("*");
			}
			System.out.println();
			etoiles+=2;
		}
		
	}

}
