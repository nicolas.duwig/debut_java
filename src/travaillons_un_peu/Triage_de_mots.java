package travaillons_un_peu;

import java.util.Arrays;
import java.util.Scanner;

//programme qui lit une suite de mot et les affiche dans l'ordre alphabetique

public class Triage_de_mots {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int nbMots;
	
		
		Scanner clavier = new Scanner(System.in);
		System.out.print("combien de mots ? ");
		nbMots = clavier.nextInt();
		String [] mots = new String [nbMots];;
		
		for (int i=0; i<nbMots; i++) {
			Scanner clavier2 = new Scanner(System.in);
			System.out.print("donnez vos mots");
			mots[i]=clavier2.nextLine();
		}
		Arrays.sort(mots);
		System.out.println("Liste par ordre alphabetique :");
		for ( int i=0; i<mots.length; i++ ) {
		    System.out.println(mots[ i ] );
		}		
	}
}
