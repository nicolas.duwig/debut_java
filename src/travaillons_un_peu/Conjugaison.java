package travaillons_un_peu;

import java.util.Scanner;

//programme qui lit un verbe et qui affiche sa conjugaison

public class Conjugaison {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//On commence par demander le nombre de lignes souhaité en saisie clavier
		Scanner clavier = new Scanner(System.in);
		System.out.print("donnez un verbe regulier du premier groupe : ");
		String verbe = clavier.nextLine();
		
		//On garde en mémoire la longueur du verbe
		int longeurVerbe=verbe.length();
		//On prend deux comparants pour vérifier si le verbe se terminer par .er
		char comparantE = verbe.charAt(longeurVerbe-2);
		char comparantR= verbe.charAt(longeurVerbe-1);
		
		//On s'assure que le verbe est terminé par er
		while  (!(comparantE == 'e' && comparantR == 'r')) {
			System.out.print("il ne se termine pas par er - donnez-en un autre");
			verbe = clavier.nextLine();
			longeurVerbe=verbe.length();
			comparantE = verbe.charAt(longeurVerbe-2);
			comparantR= verbe.charAt(longeurVerbe-1);
		}
		//on garde seuelement le radical du verbe
		verbe=verbe.substring(0,longeurVerbe-2); 
		
		String [] terminaisons = {"e", "es", "e", "ons", "ez", "ent"};
		String [] pronoms = {"je", "tu" ,"il/elle", "nous", "vous", "ils/elles"};
		
		for (int i=0; i<6; i++) {
			System.out.println(pronoms[i]+" "+verbe+terminaisons[i]);
		}
		
	}

}
